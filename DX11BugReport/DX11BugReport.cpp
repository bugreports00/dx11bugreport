// DX11BugReport.cpp : demonstrates an error (access violation) which occurs when a particular shader is passed to
//                     CreateComputeShader (DX11) through the AMD driver and contrast that with when the same is passed through
//                     WARP.

#include <wrl/client.h> // for ComPtr
#include <d3d11_1.h>
#include <stdio.h>
#include <vector>

#include "Test.hcs"

using namespace Microsoft::WRL;

static const uint32_t ShaderByteStride = 16;
static const uint32_t DispatchSizeX = 1;
static const uint32_t ThreadsPerDispatch = DispatchSizeX * 64;
static const uint32_t UavSizePerThread = 8;

// Shader will produce unique results depending on this constant.
static const uint32_t ShaderLoopIterations = 0x100;
static const uint32_t ShaderOutputUInt32Count = 4;

bool IsMicrosoftBasicRenderDriver(
    const DXGI_ADAPTER_DESC1& adapterDesc1)
{
    return adapterDesc1.VendorId == 5140 &&
        adapterDesc1.DeviceId == 140;
}

HRESULT GetAdapters(
    std::vector<ComPtr<IDXGIAdapter1>>& adapters)
{
    HRESULT hr;

    ComPtr<IDXGIFactory1> pFactory;

    CreateDXGIFactory1(
        __uuidof(IDXGIFactory1),
        reinterpret_cast<void**>(pFactory.GetAddressOf()));

    for (uint32_t i = 0;; ++i)
    {
        ComPtr<IDXGIAdapter1> pAdapter;

        hr = pFactory->EnumAdapters1(
            i,
            pAdapter.GetAddressOf());
        if (FAILED(hr))
        {
            if (hr == DXGI_ERROR_NOT_FOUND)
            {
                break;
            }

            return hr;
        }

        adapters.push_back(pAdapter);
    }

    return S_OK;
}

static void CopyBufferRegion(
    _In_ ID3D11DeviceContext* pDeviceContext,
    _In_ ID3D11Buffer* pDstBuffer,
    uint32_t dstOffset,
    _In_ ID3D11Buffer* pSrcBuffer,
    uint32_t srcOffset,
    uint32_t numBytes)
{
    uint32_t left = srcOffset;

    uint32_t right = left + numBytes;

    D3D11_BOX srcBox =
    {
        left, // left
        0, // top
        0, // front
        right, // right
        1, // bottom
        1 // back
    };

    pDeviceContext->CopySubresourceRegion(
        pDstBuffer,
        0, // dst subresource
        dstOffset, // dst X
        0, // dst Y
        0, // dst Z
        pSrcBuffer,
        0, // src subresource
        &srcBox);
}

HRESULT RunTest(
    IDXGIAdapter1* pAdapter,
    uint32_t* shaderOutput)
{
    HRESULT hr;

    UINT createDeviceFlags =
        D3D11_CREATE_DEVICE_SINGLETHREADED;

    ComPtr<ID3D11Device> pDevice;
    ComPtr<ID3D11DeviceContext> pDeviceContext;

    ComPtr<ID3D11Buffer> pCBuffer;
    ComPtr<ID3D11Buffer> pReadBuffer;
    ComPtr<ID3D11Buffer> pWriteBuffer;
    ComPtr<ID3D11Buffer> pUAVBuffer;
    ComPtr<ID3D11UnorderedAccessView> pUAVDescriptor;
    ComPtr<ID3D11ComputeShader> pShader;

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_1,
        D3D_FEATURE_LEVEL_11_0,
    };

    D3D_FEATURE_LEVEL featureLevel;

    hr = ::D3D11CreateDevice(
        pAdapter,
        D3D_DRIVER_TYPE_UNKNOWN,
        nullptr,
        createDeviceFlags,
        featureLevels,
        ARRAYSIZE(featureLevels),
        D3D11_SDK_VERSION,
        pDevice.GetAddressOf(),
        &featureLevel,
        pDeviceContext.GetAddressOf());
    if (FAILED(hr))
    {
        return hr;
    }

    {
        D3D11_BUFFER_DESC bufferDesc =
        {
            ShaderByteStride, // ByteWidth
            D3D11_USAGE_DEFAULT, // Usage
            D3D11_BIND_CONSTANT_BUFFER, // BindFlags
            0, // CPUAccessFlags
            0, // MiscFlags
            ShaderByteStride, // StructureByteStride
        };

        hr = pDevice->CreateBuffer(
            &bufferDesc,
            nullptr,
            pCBuffer.GetAddressOf());
    }
    if (FAILED(hr))
    {
        return hr;
    }

    {
        D3D11_BUFFER_DESC bufferDesc =
        {
            1 * ShaderByteStride, // ByteWidth
            D3D11_USAGE_STAGING, // Usage
            0, // BindFlags
            D3D11_CPU_ACCESS_READ, // CPUAccessFlags
            D3D11_RESOURCE_MISC_BUFFER_STRUCTURED, // MiscFlags
            ShaderByteStride, // StructureByteStride
        };

        hr = pDevice->CreateBuffer(
            &bufferDesc,
            nullptr,
            pReadBuffer.GetAddressOf());
    }
    if (FAILED(hr))
    {
        return hr;
    }

    {
        D3D11_BUFFER_DESC bufferDesc =
        {
            1 * ShaderByteStride, // ByteWidth
            D3D11_USAGE_STAGING, // Usage
            0, // BindFlags
            D3D11_CPU_ACCESS_WRITE, // CPUAccessFlags
            D3D11_RESOURCE_MISC_BUFFER_STRUCTURED, // MiscFlags
            ShaderByteStride, // StructureByteStride
        };

        hr = pDevice->CreateBuffer(
            &bufferDesc,
            nullptr,
            &pWriteBuffer);
    }
    if (FAILED(hr))
    {
        return hr;
    }

    {
        D3D11_BUFFER_DESC bufferDesc =
        {
            ThreadsPerDispatch * UavSizePerThread * ShaderByteStride, // ByteWidth
            D3D11_USAGE_DEFAULT, // Usage
            D3D11_BIND_UNORDERED_ACCESS, // BindFlags
            0, // CPUAccessFlags
            D3D11_RESOURCE_MISC_BUFFER_STRUCTURED, // MiscFlags
            ShaderByteStride, // StructureByteStride
        };

        hr = pDevice->CreateBuffer(
            &bufferDesc,
            nullptr,
            &pUAVBuffer);
    }
    if (FAILED(hr))
    {
        return hr;
    }

    {
        D3D11_UNORDERED_ACCESS_VIEW_DESC uaView =
        {
            DXGI_FORMAT_UNKNOWN, // Format
            D3D11_UAV_DIMENSION_BUFFER, // ViewDimension
        };

        uaView.Buffer.FirstElement = 0;
        uaView.Buffer.Flags = 0;
        uaView.Buffer.NumElements = ThreadsPerDispatch * UavSizePerThread;

        hr = pDevice->CreateUnorderedAccessView(
            pUAVBuffer.Get(),
            &uaView,
            pUAVDescriptor.GetAddressOf());
    }
    if (FAILED(hr))
    {
        return hr;
    }

    hr = pDevice->CreateComputeShader(
        &g_Test[0],
        sizeof(g_Test),
        nullptr,
        pShader.GetAddressOf());
    if (FAILED(hr))
    {
        return hr;
    }

    // bind
    {
        ID3D11Buffer* constantBuffers[] = {
            pCBuffer.Get(),
        };

        pDeviceContext->CSSetConstantBuffers(
            0,
            _countof(constantBuffers),
            constantBuffers);
    }

    // Write to constant buffer
    D3D11_MAPPED_SUBRESOURCE mappedResource;

    hr = pDeviceContext->Map(
        pWriteBuffer.Get(),
        0,
        D3D11_MAP_WRITE,
        0,
        &mappedResource);
    if (FAILED(hr))
    {
        return hr;
    }

    {
        volatile uint32_t* p32 = reinterpret_cast<volatile uint32_t*>(
            mappedResource.pData);

        p32[0] = ShaderLoopIterations;
        p32[1] = 0;
        p32[2] = 0;
        p32[3] = 0;
    }

    pDeviceContext->Unmap(
        pWriteBuffer.Get(),
        0);

    CopyBufferRegion(
        pDeviceContext.Get(),
        pCBuffer.Get(),
        0,
        pWriteBuffer.Get(),
        0,
        1 * ShaderByteStride);

    ID3D11UnorderedAccessView* unorderedAccessViews[] = {
        pUAVDescriptor.Get(),
    };

    pDeviceContext->CSSetUnorderedAccessViews(
        0,
        _countof(unorderedAccessViews),
        unorderedAccessViews,
        nullptr);

    pDeviceContext->CSSetShader(
        pShader.Get(),
        nullptr,
        0);

    pDeviceContext->Dispatch(
        DispatchSizeX,
        1,
        1);

    // Read from UAV
    CopyBufferRegion(
        pDeviceContext.Get(),
        pReadBuffer.Get(),
        0 * ShaderByteStride,
        pUAVBuffer.Get(),
        0,
        1 * ShaderByteStride);

    hr = pDeviceContext->Map(
        pReadBuffer.Get(),
        0,
        D3D11_MAP_READ,
        0,
        &mappedResource);
    if (FAILED(hr))
    {
        return hr;
    }

    ::memcpy(
        shaderOutput,
        mappedResource.pData,
        ShaderByteStride);

    // Place a breakpoint here to check output--Should be equal with both adapters, regardless of second argument.
    pDeviceContext->Unmap(
        pReadBuffer.Get(),
        0);

    return hr;
}

int main()
{
    HRESULT hr = E_UNEXPECTED;

    std::vector<ComPtr<IDXGIAdapter1>> adapters;

    hr = GetAdapters(adapters);
    if (FAILED(hr))
    {
        goto Error;
    }

    uint32_t expectedResult[ShaderOutputUInt32Count];

    for (uint32_t i = 0;; ++i)
    {
        if (i == adapters.size())
        {
            printf("Microsoft Basic Render Driver not found!\n");

            hr = E_FAIL;
            goto Error;
        }

        DXGI_ADAPTER_DESC1 adapterDesc1;

        hr = adapters[i]->GetDesc1(&adapterDesc1);
        if (FAILED(hr))
        {
            goto Error;
        }

        // Find Microsoft Basic Render Driver
        if (!IsMicrosoftBasicRenderDriver(adapterDesc1))
        {
            continue;
        }

        printf("Testing %S...", adapterDesc1.Description);

        hr = RunTest(
            adapters[i].Get(),
            expectedResult);
        if (FAILED(hr))
        {
            goto Error;
        }

        printf("PASS!\n");

        break;
    }

    uint32_t actualResult[ShaderOutputUInt32Count];

    for (uint32_t i = 0; i != adapters.size(); ++i)
    {
        DXGI_ADAPTER_DESC1 adapterDesc1;

        hr = adapters[i]->GetDesc1(&adapterDesc1);
        if (FAILED(hr))
        {
            goto Error;
        }

        // Find non-Microsoft Basic Render Drivers
        if (IsMicrosoftBasicRenderDriver(adapterDesc1))
        {
            continue;
        }

        printf("Testing %S...", adapterDesc1.Description);

        hr = RunTest(
            adapters[i].Get(),
            actualResult);
        if (FAILED(hr))
        {
            goto Error;
        }

        if (::memcmp(
            expectedResult,
            actualResult,
            sizeof(expectedResult)) != 0)
        {
            printf("FAIL: Result mismatch\n");

            continue;
        }

        printf("PASS!\n");
    }

Error:
    if (hr != S_OK)
    {
        printf("ERROR: hr=0x%08x\n", hr);
    }

    return 0;
}

